/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javanotadadisciplina;

/**
 *
 * @author Raynan
 */
public class Aluno {
    private String nome;
    private double[] notasHist= new double[3];
    private double[] notas = new double[3];
    private boolean isNotas=false;

    public boolean isIsNotas() {
        return isNotas;
    }

    public void setIsNotas(boolean isNotas) {
        this.isNotas = isNotas;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double[] getNotasHist() {
        return notasHist;
    }

    public void setNotasHist(double[] notasHist) {
        this.notasHist = notasHist;
    }

    public double[] getNotas() {
        return notas;
    }

    public void setNotas(double[] notas) {
        this.notas = notas;
    }

    public Aluno() {
    }
    
}
