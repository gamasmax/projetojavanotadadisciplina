/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javanotadadisciplina;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Raynan
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private TextField nome;
    @FXML
    private TextField prog1Tri1;
    @FXML
    private TextField prog1Tri2;
    @FXML
    private TextField prog1Tri3;
    @FXML
    private TextField prog2Tri1;
    @FXML
    private TextField prog2Tri2;
    @FXML
    private TextField prog2Tri3;
    @FXML
    private TextField chamada;
    @FXML
    private TextField chamadaHist;
    @FXML
    private Button cadAluno;
    
    Aluno[] Alunos = new Aluno[25];
    int i=0;
    @FXML
    private void cadastrarAluno() {
        if (i>24){
            cadAluno.setText("Numero máximo de alunos chegado");
        }
        else{
            Alunos[i]=new Aluno();
            Alunos[i].setNome(nome.getText());
            double[] notasHist = new double[3];
                notasHist[0] = Double.parseDouble(prog1Tri1.getText());
                notasHist[1] = Double.parseDouble(prog1Tri2.getText());
                notasHist[2] = Double.parseDouble(prog1Tri3.getText());
            Alunos[i].setNotasHist(notasHist);
            int cham=i;
            System.out.println("Aluno " + Alunos[i].getNome() + " Criado com sucesso, numero da chamada " + ++cham);
            i++;
        }
    }
    @FXML
    private void cadastrarNotas(){
       int leChamada=Integer.parseInt(chamada.getText());
       double[] notas = new double[3];
                notas[0] = Double.parseDouble(prog2Tri1.getText());
                notas[1] = Double.parseDouble(prog2Tri2.getText());
                notas[2] = Double.parseDouble(prog2Tri3.getText());
       Alunos[leChamada-1].setNotas(notas);
       Alunos[leChamada-1].setIsNotas(true);
       System.out.println("Notas botadas com sucesso");
    }
    @FXML
    private void mostrarHistorico(){
       int leChamada=Integer.parseInt(chamadaHist.getText());
       System.out.println("Nome: " + Alunos[leChamada-1].getNome());
       System.out.println("Nota prog1 tri1: " + Alunos[leChamada-1].getNotasHist()[0]);
       System.out.println("Nota prog1 tri2: " + Alunos[leChamada-1].getNotasHist()[1]);
       System.out.println("Nota prog1 tri3: " + Alunos[leChamada-1].getNotasHist()[2]);
       System.out.println("Nota prog2 tri1: " + Alunos[leChamada-1].getNotas()[0]);
       System.out.println("Nota prog2 tri2: " + Alunos[leChamada-1].getNotas()[1]);
       System.out.println("Nota prog2 tri3: " + Alunos[leChamada-1].getNotas()[2]);
    }
    @FXML
    private void mostrarTurma(){
       for (int j=0; j<i; j++){
           if (Alunos[j].isIsNotas()==true){
       System.out.println("Nome: " + Alunos[j].getNome());
       System.out.println("Nota prog2 tri1: " + Alunos[j].getNotas()[0]);
       System.out.println("Nota prog2 tri2: " + Alunos[j].getNotas()[1]);
       System.out.println("Nota prog2 tri3: " + Alunos[j].getNotas()[2]);
       System.out.println("");
           }
    }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
